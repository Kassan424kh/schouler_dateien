# Bitte in dieser Programm nichts Ändern
# alle Rechte sind Bei Khalil Khalil vorbehalten
# und nichts kopieren




# Flask Tools --------------------------------------------
from flask import Flask, render_template, request, flash, redirect, url_for, session

# Mysql Tools ---------------------------------------------
from flask_mysqldb import MySQL

# Form Models ---------------------------------------------
from wtforms import Form, StringField, TextAreaField, validators


# Zufallsgeneratur ----------------------------------------
import random
random.seed()

# File Upload ---------------------------------------------
import os
from werkzeug.utils import secure_filename
app = Flask(__name__)


#----------------------------------------------------------------------------------- Verbindung mit DatenBanken
# Config MySQL
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'K1982301halil'
app.config['MYSQL_DB'] = 'schouler_dateien'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'
#init MYSQL
mysql = MySQL(app)




#----------------------------------------------------------------------------------- The add schouler
# Alle meine Klassen
@app.route('/')
def klassen():
    # Create Cursor
    cur = mysql.connection.cursor()

    # Execute
    cur.execute('SELECT * FROM klassen WHERE 1')
    klassen = cur.fetchall()

    # Commit to DB
    mysql.connection.commit()

    # Close connection
    cur.close()
    return render_template('klassen.html', klassen = klassen)



# add_klassen Form Class
class klassen(Form):
    klassenname = StringField(' ', [validators.DataRequired('keine Aingabe')])

# Add klassen Site
@app.route('/klassen/add_klasse', methods=['GET', 'POST'])
def add_klasse():
    form = klassen(request.form)
    if request.method == 'POST' and form.validate():
        klassenname = form.klassenname.data

        # Create Cursor
        cur = mysql.connection.cursor()

        # Execute

        cur.execute('INSERT INTO klassen(klassenname) VALUES (%s)', [klassenname])
        flash('Klasse ist erstellt', 'success')

        # Commit to DB
        mysql.connection.commit()

        # Close connection
        cur.close()
        # Bake to Groups Site if the Group success created
        return redirect(url_for('klassen'))
    return render_template('add_klasse.html', form=form)



# edit klasse
@app.route('/edit_klasse/<string:klasse_id>', methods=['GET', 'POST'])
def edit_klasse(klasse_id):
    form = klassen(request.form)
    if request.method == 'POST' and form.validate():
        klassenname = form.klassenname.data

        #Create Cursor
        cur = mysql.connection.cursor()

        # Execute
        cur.execute('UPDATE klassen SET klassenname = %s WHERE klassen.klasse_id = %s;', (klassenname, klasse_id))

        # Commit connection
        cur.connection.commit()

        #Close connection
        cur.close()

        flash('Klasse ist aktualliesiert', 'success')
        return redirect(url_for('klassen'))
    return render_template('edit_klasse.html', form=form)


# Add schouler Site
@app.route('/louschen_klasse/<string:klasse_id>', methods=['GET', 'POST'])
def louschen_klasse(klasse_id):
        # Create Cursor
    cur = mysql.connection.cursor()

    # Execute

    cur.execute('UPDATE klassen SET wenn_klasse_gelouscht = "True" WHERE klasse_id LIKE %s', [klasse_id])


    # Commit to DB
    mysql.connection.commit()

    # Close connection
    cur.close()
    flash('Klasse ist gelöscht', 'success')
    return redirect(url_for('klassen'))


# Add klasse Site
@app.route('/wiederherstellen_klasse/<string:klasse_id>', methods=['GET', 'POST'])
def wiederherstellen_klasse(klasse_id):
        # Create Cursor
    cur = mysql.connection.cursor()

    # Execute

    cur.execute('UPDATE klassen SET wenn_klasse_gelouscht = "False" WHERE klasse_id LIKE %s', [klasse_id])


    # Commit to DB
    mysql.connection.commit()

    # Close connection
    cur.close()
    flash('Klasse ist wiederhergestellt', 'success')
    return redirect(url_for('klassen'))


# Add schouler Site
@app.route('/louschen_klasse_four_immer/<string:klasse_id>', methods=['GET', 'POST'])
def louschen_klasse_four_immer(klasse_id):
        # Create Cursor
    cur = mysql.connection.cursor()
    result_schouler_dieser_klasse = cur.execute('SELECT * FROM schouler WHERE schouler.klassen_id LIKE %s', [klasse_id])
    schouler_dieser_klasse = cur.fetchall()

# Execute
    for schouler in schouler_dieser_klasse:
        cur.execute('DELETE FROM beschreibung WHERE beschreibung.schouler_id LIKE %s', [schouler['schouler_id']])
        cur.execute('DELETE FROM schouler WHERE schouler_id LIKE %s', [schouler['schouler_id']])
    cur.execute('DELETE FROM klassen WHERE klassen.klasse_id LIKE %s', [klasse_id])
    # Commit to DB
    mysql.connection.commit()

    # Close connection
    cur.close()
    flash('Klasse ist gelöscht für immer', 'success')
    return redirect(url_for('klassen'))









#----------------------------------------------------------------------------------- The add schouler
# schouler dieser Klasse
@app.route('/<string:klassenname>/<string:klasse_id>/schouler')
def schouler(klassenname, klasse_id):
    klassen_datein = {'klassenname': klassenname, 'klasse_id': klasse_id}
    session['klasse_id'] = klasse_id
    session['klassenname'] = klassenname

    # Create Cursor
    cur = mysql.connection.cursor()

    # Execute
    result = cur.execute('SELECT * FROM schouler WHERE klassenname LIKE %s', [klassenname])

    if result <= 0:
        return redirect('/' + klassenname + '/' + klasse_id+'/add_schouler')
    else:
        schouler = cur.fetchall()

    # Commit to DB
    mysql.connection.commit()

    # Close connection
    cur.close()
    return render_template('schouler.html', schouler=schouler, klassenname = klassen_datein)


#----------------------------------------------------------------------------------- The add schouler


# add_schouler Form Class
class schouler_form(Form):
    vorname = StringField(' ', [validators.DataRequired('keine Aingabe')])
    nachname = StringField(' ', [validators.DataRequired('keine Aingabe')])
    gebortsdatum = StringField(' ', [validators.DataRequired('keine Aingabe')])



# schouler Images Upload -----------------------------
UPLOAD_FOLDER = './static/schouler/'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS



# Add schouler Site
@app.route('/<string:KName>/<string:KId>/add_schouler', methods=['GET', 'POST'])
def add_schouler(KName, KId):
    form = schouler_form(request.form)
    if request.method == 'POST' and form.validate():
        # Profilbild für der/die Schüler hochladen
        if 'file' not in request.files:
            # When the User a False File (not a Bild) shoosed
            filename = 'NULL'
        try:
            # When the User a Photo shoosed
            file = request.files['file']
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                file.save(app.config['UPLOAD_FOLDER'] + str(filename))
        except:
            pass
        vorname = form.vorname.data
        nachname = form.nachname.data
        gebortsdatum = form.gebortsdatum.data
        klassenname = KName
        klasse_id = KId

        # Create Cursor
        cur = mysql.connection.cursor()

        # Execute
        try:
            cur.execute('INSERT INTO schouler(vorname, nachname, gebortsdatum, klassenname, klassen_id, schouler_bild) '
                        'VALUES(%s, %s, %s, %s, %s, %s)',
                        (vorname, nachname, gebortsdatum, klassenname, klasse_id, filename))
            flash('schouler ist erstellt', 'success')
        except:
            flash('Einige Felder sind nicht ausgefült', 'error')
            flash('Oder haben Sie ein Falshe "kein Bild" Datei hochgeladen', 'error')
            flash('Bitte fühlen Sie alle Felder aus, und füge ein Richtiges Bild ein', 'msg')
            return redirect('/' + KName + '/' + session['klasse_id'] + '/add_schouler')


        # Commit to DB
        mysql.connection.commit()

        # Close connection
        cur.close()
        # Bake to Groups Site if the Group success created
        return redirect(url_for('klassen'))
    return render_template('add_schouler.html', form=form)




# Add schouler Site
@app.route('/<string:klassenname>/louschen_schouler/<string:schouler_id>', methods=['POST'])
def louschen_schouler(klassenname, schouler_id):
        # Create Cursor
    cur = mysql.connection.cursor()

    # Execute

    cur.execute('UPDATE schouler SET wenn_schouler_gelouscht = "True" WHERE schouler_id LIKE %s', [schouler_id])


    # Commit to DB
    mysql.connection.commit()

    # Close connection
    cur.close()
    flash('Schöler ist gelöscht', 'success')
    return redirect('/'+ klassenname + '/'+ session['klasse_id'] +'/schouler')


# Add schouler Site
@app.route('/<string:klassenname>/wiederherstellen_schouler/<string:schouler_id>', methods=['GET', 'POST'])
def wiederherstellen_schouler(klassenname, schouler_id):
        # Create Cursor
    cur = mysql.connection.cursor()

    # Execute

    cur.execute('UPDATE schouler SET wenn_schouler_gelouscht = "False" WHERE schouler_id LIKE %s', [schouler_id])


    # Commit to DB
    mysql.connection.commit()

    # Close connection
    cur.close()
    flash('Schöler ist wiederhergestellt', 'success')
    return redirect('/' + klassenname + '/' + session['klasse_id'] + '/schouler')


# Add schouler Site
@app.route('/<string:klassenname>/louschen_schouler_four_immer/<string:schouler_id>', methods=['GET', 'POST'])
def louschen_schouler_four_immer(klassenname, schouler_id):
        # Create Cursor
    cur = mysql.connection.cursor()

    # Execute

    cur.execute('DELETE FROM schouler WHERE schouler.schouler_id LIKE %s', [schouler_id])
    cur.execute('DELETE FROM beschreibung WHERE beschreibung.schouler_id LIKE %s', [schouler_id])

    # Commit to DB
    mysql.connection.commit()

    # Close connection
    cur.close()
    flash('Schöler ist gelöscht für immer', 'success')
    return redirect('/'+ klassenname + '/'+ session['klasse_id'] +'/schouler')


#----------------------------------------------------------------------------------- The add schouler
# schouler dieser Klasse
@app.route('/<string:klassenname>/<string:klasse_id>/schouler/<schouler_id>/', methods=['GET', 'POST'])
def beschreibung(klassenname, klasse_id, schouler_id):

    # Create Cursor
    cur = mysql.connection.cursor()

    # Execute
    result = cur.execute('SELECT * FROM beschreibung WHERE schouler_id LIKE %s', [schouler_id])
    beschreibung = cur.fetchone()

    b_id = beschreibung
    if b_id ==  None:
        return redirect(klassenname + '/add_beschreibung/' + schouler_id )
    else:
        b_id = beschreibung['b_id']
        return redirect(klassenname + '/edit_beschreibung/' + schouler_id + '/' + str(b_id))

    # Commit to DB
    mysql.connection.commit()

    # Close connection
    cur.close()
    return render_template('beschreibung.html')


#----------------------------------------------------------------------------------- The add schouler
# beschreibung Form Class
class beschreibung_form(Form):
    beschreibung = TextAreaField(' ', [validators.DataRequired('keine Angebe ...')])

# Add schouler Site
@app.route('/<string:klassenname>/add_beschreibung/<schouler_id>', methods=['GET', 'POST'])
def add_beschreibung(klassenname, schouler_id):
    cur = mysql.connection.cursor()
    # Execute
    cur.execute('SELECT * FROM schouler WHERE schouler_id = %s', [schouler_id])
    schouler_daten = cur.fetchone()
    mysql.connection.commit()
    # Close connection
    cur.close()
    form = beschreibung_form(request.form)
    if request.method == 'POST' and form.validate():

        # Create Cursor
        cur = mysql.connection.cursor()
        beschreibung = request.form['beschreibung']
        # Execute
        cur.execute('INSERT INTO beschreibung(beschreibung, schouler_id) VALUES(%s, %s)', (beschreibung, schouler_id))
        flash('beschreibung ist neu erstellt', 'success')

        # Commit to DB
        mysql.connection.commit()

        # Close connection
        cur.close()
        # Bake to Groups Site if the Group success created
        return redirect(klassenname + '/' + session['klasse_id'] +  '/schouler')
    return render_template('beschreibung.html', form = form, schouler_daten = schouler_daten, klassenname = klassenname)



@app.route('/<string:klassenname>/edit_beschreibung/<schouler_id>/<int:b_id>', methods=['GET', 'POST'])
def edit_beschreibung(klassenname, schouler_id, b_id):
    cur = mysql.connection.cursor()
    # Execute
    cur.execute('SELECT * FROM schouler WHERE schouler_id = %s', [schouler_id])

    schouler_daten = cur.fetchone()

    mysql.connection.commit()


    # Close connection
    cur.close()
    cur = mysql.connection.cursor()
    # Execute
    result = cur.execute('SELECT * FROM beschreibung WHERE schouler_id = %s ', [schouler_id])
    beschreibung = cur.fetchone()

    # Close connection
    cur.close()

    # get Form
    form = beschreibung_form(request.form)
    form.beschreibung.data = beschreibung['beschreibung']

    if request.method == 'POST' and form.validate():
        beschreibung = request.form['beschreibung']
        # Create Cursor
        cur = mysql.connection.cursor()
        # Execute
        cur.execute('UPDATE beschreibung SET beschreibung = %s WHERE beschreibung.b_id = %s', [beschreibung, b_id])
        # Commit to DB
        mysql.connection.commit()

        # Close connection
        cur.close()

        flash('Beschribung ist aktualliesiert', 'success')
        # Bake to Groups Site if the Group success created
        return redirect(klassenname + '/' + session['klasse_id'] + '/schouler')
    return render_template('edit_beschreibung.html', form=form, schouler_daten = schouler_daten, klassenname = klassenname)


# ----------------------------------------------------------------------------------- start der Server
# Start the Server
if __name__== '__main__':
    app.secret_key = 'asfd\jf'
    app.run(debug=True, host='000')


