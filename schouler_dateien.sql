-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Erstellungszeit: 21. Apr 2018 um 11:42
-- Server-Version: 5.7.17-log
-- PHP-Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `schouler_dateien`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `beschreibung`
--

CREATE TABLE `beschreibung` (
  `b_id` int(11) NOT NULL,
  `beschreibung` text COLLATE utf8_bin NOT NULL,
  `schouler_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `klassen`
--

CREATE TABLE `klassen` (
  `klasse_id` int(11) NOT NULL,
  `klassenname` varchar(200) COLLATE utf8_bin NOT NULL,
  `wenn_klasse_gelouscht` varchar(5) COLLATE utf8_bin NOT NULL DEFAULT 'False'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `schouler`
--

CREATE TABLE `schouler` (
  `schouler_id` int(11) NOT NULL,
  `vorname` varchar(100) COLLATE utf8_bin NOT NULL,
  `nachname` varchar(100) COLLATE utf8_bin NOT NULL,
  `gebortsdatum` varchar(10) COLLATE utf8_bin NOT NULL,
  `klassenname` varchar(200) COLLATE utf8_bin NOT NULL,
  `klassen_id` int(11) NOT NULL,
  `schouler_bild` varchar(350) COLLATE utf8_bin NOT NULL,
  `wenn_schouler_gelouscht` varchar(5) COLLATE utf8_bin NOT NULL DEFAULT 'False'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `beschreibung`
--
ALTER TABLE `beschreibung`
  ADD PRIMARY KEY (`b_id`);

--
-- Indizes für die Tabelle `klassen`
--
ALTER TABLE `klassen`
  ADD PRIMARY KEY (`klasse_id`);

--
-- Indizes für die Tabelle `schouler`
--
ALTER TABLE `schouler`
  ADD PRIMARY KEY (`schouler_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `beschreibung`
--
ALTER TABLE `beschreibung`
  MODIFY `b_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT für Tabelle `klassen`
--
ALTER TABLE `klassen`
  MODIFY `klasse_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT für Tabelle `schouler`
--
ALTER TABLE `schouler`
  MODIFY `schouler_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
